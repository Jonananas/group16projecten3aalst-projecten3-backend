var mongoose = require('mongoose');

var ChallengeSchema = new mongoose.Schema({
  title: String,
  tijdtype: String,
  challengetype: String
});

mongoose.model('Challenge', ChallengeSchema);
