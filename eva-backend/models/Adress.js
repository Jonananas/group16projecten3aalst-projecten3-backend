var mongoose = require('mongoose');

var AdressSchema = new mongoose.Schema({
  straat: String,
  nummer: String,
  postcode: String,
  plaats: String,
  bus: String
});

mongoose.model('Adress', AdressSchema);
