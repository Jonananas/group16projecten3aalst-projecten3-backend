var mongoose = require('mongoose');
var crypto = require('crypto');
var jwt = require('jsonwebtoken');

var GebruikerSchema = new mongoose.Schema({
  login: {type: String, lowercase: true, unique: true},
  hash: String,
  salt: String,
  //naam: String,
  //voornaam: String,
  achievements: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Achievement' }]
});

GebruikerSchema.methods.setPassword = function(password){
  this.salt = crypto.randomBytes(16).toString('hex');

  this.hash = crypto.pbkdf2Sync(password, this.salt, 1000, 64).toString('hex');
};

GebruikerSchema.methods.validPassword = function(password) {
  var hash = crypto.pbkdf2Sync(password, this.salt, 1000, 64).toString('hex');

  return this.hash === hash;
};

GebruikerSchema.methods.generateJWT = function() {


  var today = new Date();
  var exp = new Date(today);
  exp.setDate(today.getDate() + 60);

  return jwt.sign({
    _id: this._id,
    login: this.login,
    exp: parseInt(exp.getTime() / 1000),
  }, 'SECRET');
};

mongoose.model('Gebruiker', GebruikerSchema);
