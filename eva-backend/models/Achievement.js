var mongoose = require('mongoose');

var AchievementSchema = new mongoose.Schema({
  titel: String,
  afbeeldingUrl: String
});

mongoose.model('Achievement', AchievementSchema);
