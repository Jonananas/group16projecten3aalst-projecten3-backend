var express = require('express');
var router = express.Router();
var passport = require('passport');
var mongoose = require('mongoose');
var Gebruiker = mongoose.model('Gebruiker');
var jwt = require('express-jwt');
var auth = jwt({secret: 'SECRET', GebruikerProperty: 'payload'});


router.param('achievement', function(req, res, next, id) {
  var query = Achievement.findById(id);
  query.exec(function (err, achievement){
    if (err) { return next(err); }
    if (!achievement) { return next(new Error('can\'t find achievement')); }
    req.achievement = achievement;
    return next();
  });
});

router.param('challenge', function(req, res, next, id) {
var query = Challenge.findById(id);
  query.exec(function (err, challenge){
    if (err) { return next(err); }
    if (!challenge) { return next(new Error('can\'t find challenge')); }
    req.challenge = challenge;
    return next();
  });
});

router.param('gebruiker', function(req, res, next, id) {
  var query = Gebruiker.findById(id);
  query.exec(function (err, gebruiker){
    if (err) { return next(err); }
    if (!gebruiker) { return next(new Error('can\'t find gebruiker')); }
    req.gebruiker = gebruiker;
    return next();
  });
});

/* GETS */

router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

router.get('/achievements', function(req, res, next) {
  Achievement.find(function(err, achievements){
    if(err){return next(err);}
    res.json(achievements);
  });
});

router.get('/challenges', function(req, res, next) {
  Challenge.find(function(err, challenges){
    if(err){return next(err);}
    res.json(challenges);
  });
});

router.get('/achievements/:achievement', function(req, res) {
  res.json(req.achievement);
});

router.get('/challenges/:challenge', function(req, res) {
  res.json(req.challenge);
});

router.get('/gebruikers/:gebruiker', function(req, res) {
  res.json(req.gebruiker);
});

/* POSTS */
router.post('/achievements', function(req, res, next) {
  var achievement = new Achievement(req.body);
  achievement.save(function(err, post){
    if(err){ return next(err); }
    res.json(achievement);
  });
});

router.post('/challenges', function(req, res, next) {
  var challenge = new Challenge(req.body);
  challenge.save(function(err, post){
    if(err){ return next(err); }
    res.json(challenge);
  });
});

router.post('/register', function(req, res, next){
  if(!req.body.username || !req.body.password){
    return res.status(400).json({message: 'Please fill out all fields'});
  }
  var gebruiker = new Gebruiker();
  gebruiker.login = req.body.username;
  gebruiker.setPassword(req.body.password);
  gebruiker.save(function (err){
    if(err){ return next(err); }
    return res.json({token: gebruiker.generateJWT()});
  });
});

router.post('/login', function(req, res, next){
  console.log(req.body);
  if(!req.body.username || !req.body.password){
    return res.status(400).json({message: 'Please fill out all fields'});
  }
  passport.authenticate('local', function(err, gebruiker, info){
    console.log('gebruiker: ', gebruiker);
    if(err){ return next(err); }
    if(gebruiker){
      return res.json({token: gebruiker.generateJWT()});
    } else {
      return res.status(401).json(info);
    }
  })(req, res, next);
});

// =====================================
// FACEBOOK ROUTES =====================
// =====================================
// route for facebook authentication and login
router.get('/auth/facebook', passport.authenticate('facebook', { scope : 'email' }));

// handle the callback after facebook has authenticated the user
router.get('/auth/facebook/callback',
    passport.authenticate('facebook', {
        successRedirect : '/profile',
        failureRedirect : '/'
    }));

// route for logging out
router.get('/logout', function(req, res) {
    req.logout();
    res.redirect('/');
});



// route middleware to make sure a user is logged in
function isLoggedIn(req, res, next) {

// if user is authenticated in the session, carry on
if (req.isAuthenticated())
    return next();

// if they aren't redirect them to the home page
res.redirect('/');
}

/*
//facebook Login gedeelte

//facebook login
router.get('/login/facebook',
  passport.authenticate('facebook', { scope : 'email' }
));

// callback voor na facebook login
router.get('/login/facebook/callback',
  passport.authenticate('facebook', {
    successRedirect : '/home',
    failureRedirect : '/'
  })
);
*/

module.exports = router;
function isLoggedIn(req, res, next) {

// if user is authenticated in the session, carry on
if (req.isAuthenticated())
    return next();

// if they aren't redirect them to the home page
res.redirect('/');
}
